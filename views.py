import json
import django.http
import django.forms.util
import django.db
import django.views.generic
import django.conf
from django.core.urlresolvers import reverse_lazy, reverse

class PaginatedView(django.views.generic.View):
    def prepare_paginated_context( self, request ):
        pagination_details = self.get_pagination_details(request)
        # Load the credits details
        c = {}
        
        objects = self.get_queryset(request)
        
        # Try to order_by (when it is a queryset)
        if pagination_details["queryset_order"] is not None:
            try:
                objects = objects.order_by(pagination_details["queryset_order"])
                
            except AttributeError as e:
                # Not a queryset, must be a list
                # Is order by implemented?
                try:
                    self.order_by(request)
                except NotImplementedError as e:
                    # Not implemented, ignore and continue
                    pass
                except:
                    objects = sorted(objects, key=lambda x: self.order_by(request, item=x, key=pagination_details["queryset_order"]))
        
        p = django.core.paginator.Paginator(objects, self.per_page())
        try:
            c["page_object"] = p.page(pagination_details["page"])
        except django.core.paginator.EmptyPage as e:
            c["page_object"] = p.page(1)
        # Note: the paginator object could be taken from page_object.paginator in the template
        c["paginator"] = p
        c["pagination_state"] = pagination_details
        return c
        
    def get_queryset(self, request):
        # Force to implement
        raise NotImplementedError
        
    def per_page( self ):
        return django.conf.settings.DEFAULT_PAGINATION
        
    def order_by(self, request, **kwargs):
        # Force to implement (if list is returned by get_queryset only)
        raise NotImplementedError

    def get_pagination_details( self, request ):
        page = request.GET.get("page",0)
        order = request.GET.get("order", None)
        order_dir = request.GET.get("orderDir","desc")
        if order is not None:
            if order_dir == "desc":
                queryset_order = "-%s" % order
            else:
                queryset_order = order
        else:
            queryset_order = None
            
        return {
            "page": page,
            "order": order,
            "order_dir": order_dir,
            "queryset_order": queryset_order,
        }